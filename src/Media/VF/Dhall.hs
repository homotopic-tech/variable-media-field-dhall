-- |
--   Module    : Media.VF.Dhall
--   License   : MIT
--   Stability : experimental
--
-- Dhall instances for `VF`.
module Media.VF.Dhall where

import Dhall
import Media.VF

instance FromDhall u => FromDhall (VF u)

instance ToDhall u => ToDhall (VF u)

instance FromDhall u => FromDhall (VFC u)

instance ToDhall u => ToDhall (VFC u)
